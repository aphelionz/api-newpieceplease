var nodeio = require("node.io"),
    config = require("../lib/config"),
    redis = require("redis"),
    util = require("../lib/util");

var client = redis.createClient(config.redis.port, config.redis.host);
client.auth(config.redis.password);

var ids = util.range(30590,200000,1);
var piece_count = 0;
      
exports.job = new nodeio.Job({
    input: ids,
    run: function(id) {
      var API_BASE = 'http://imslp.org/api.php?format=json&action=parse&pageid=';
      var SITE_BASE = 'http://imslp.org/wiki/';
      var IMAGE_REGEX = /Special:ImagefromIndex(.*?)Complete Score/;
      var LINK_REGEX = /(http:.*?)\"/;
      var piece = {};
      
      this.get(API_BASE + id, function(err, data) {
        data = JSON.parse(data);
        var page = data.parse;
       
        if("undefined" === typeof data.error) {
          if("undefined" !== page.categories) {
            for(j in page.categories) {
              if(page.categories[j]["*"] === "For_1_player") {
                piece_count++;
                
                if(page.text['*'].match(IMAGE_REGEX)) {
                  html_snippet = SITE_BASE + page.text['*'].match(IMAGE_REGEX)[0];

                  piece.id = id
                  piece.name = page.title
                  piece.url = html_snippet.match(LINK_REGEX)[1];
                  piece.url = piece.url.replace("ImagefromIndex","IMSLPDisclaimerAccept")

                  this.emit(piece_count + ':' + id + ' - ' + piece.name + ' (' + piece.url + ')');

                  for(k in page.categories) {
                    if(page.categories[k]['*'].match(/For_|Scores_featuring_the/) && !page.categories[k]['*'].match(/For_(.*?)_player/)) {
                      client.sadd(page.categories[k]['*'].toLowerCase(), JSON.stringify(piece));
                    }
                    
                  }
                } else {
                  this.skip();
                }
              } else {
                this.skip();
              }
            }
          } else {
            this.skip();
          }
        } else {
          this.skip();
        }
      });
    }
  });