var nodeio = require("node.io"),
    config = require("../lib/config"),
    redis = require("redis"),
    util = require("../lib/util");

// Connect to redis with config values
var client = redis.createClient(config.redis.port, config.redis.host);
client.auth(config.redis.password);

var ids = util.range(1,52000,1);
var base = 'http://www.free-scores.com/download-sheet-music.php?pdf=';
var pdfbase = 'http://www.free-scores.com/include_fr/ajax/pdf_viewer.php?repvpdf=1&langue_site=en&ip=209.6.67.137&CLEF=';

exports.job = new nodeio.Job({
    input: ids,
    run: function(id) {
	  var data = {};
	  
	  this.getHtml(base + id, function(err, $) {
		  var title;
		  title = $('title').text;
		  
		  if(null !== title.match(/\(([^)]+)\)/)) {
			var title, instrument;
			
			instrument = title.match(/\(([^)]+)\)/)[1].trim();
			
			//data.id = id;
			//data.title = $('title').text;
			//data.instrument = title.match(/\(([^)]+)\)/)[1].trim();
			
			data.composer = title.match(/\:([^-]+)\-/)[1].trim();
			data.name = title.match(/\-([^(]+)\(/)[1].trim();
		  
			this.getHtml(pdfbase + id, function(err, $) {
			  data.url = $('a.g')[0].attribs.href;
					  
			  client.set("freescores:ticker", id);
			  client.sadd(instrument.toLowerCase(), JSON.stringify(data));
			  this.emit(data);
			});
		  } else {
			  this.skip();
		  }  
	  });
	}
});
