exports = {};

// The maximum number of threads (calls to run()) allowed to run concurrently, per process.
// When scraping, this can be used to limit the number of concurrent requests.
exports.max = 1;

// How many elements of input to send to each thread. If this is greater than 1, run() will receive an array
exports.take = 1;

// The maximum number of times an element (or elements) of input can be retried using retry() before the thread
// fails and fail() is called
exports.retries = 2;

// Specifies an amount of time in seconds to wait between threads. Useful if the API/server you are scraping defines
// a limit of how many requests you can make in a given amount of time. You can use this option along with the max
// option to limit the number of concurrent requests. Note that this will also wait when you call skip().
// exports.wait;

// When this is set to true, failed requests or threads that throw an exception will automatically call this.retry()
exports.auto_retry = false;

// The maximum amount of time (in seconds) each thread can run for before fail() is called
exports.timeout = false;

// The maximum amount of time (in seconds) the entire job has to complete before it exits with an error. This
// option can also be set from the command line using the -t or --timeout switch
exports.global_timeout = false;

// When calling emit() with an array argument, this option determines whether the array is flattened before being output
exports.flatten = true;

// If this is true, node.io outputs benchmark information on a job's completion: 1) completion time, 2) bytes read +
// speed, 3) bytes written + speed. This can also be enabled from the command line using the -b or --benchmark switch
exports.benchmark = false;

// EDIT: Currently broken - fix coming soon.
// Whether to use child processes to distribute processing. Set this to the number of desired workers. This can also
// be enabled from the command line using the -f or --fork switch. Run node.io --help for details.
exports.fork = false;

// This option is used to set a limit on how many lines / rows / elements are input before forcing a job to complete
// exports.input = false;

// If input is a directory, this option is used to recurse through each subdirectory.
exports.recurse = false;

// The read buffer to use when reading files
exports.read_buffer = 8096;

// The char to use as newline when outputting data. Note that input newlines are automatically detected as \n or \r\n
// exports.newline;

// The encoding to use when reading and writing data
exports.encoding = 'utf8';

// Whether to use JSDOM to parse HTML (default is to use node-htmlparser).
// If JSDOM is used, jQuery is used as the default $ object
exports.jsdom = false;

// If you set jsdom to true and want to fetch and process external Javascript files, set external_resources to
// ['script']. Other values will not work.
exports.external_resources = false;

// All requests will be made through this proxy. Alternatively, you can specify a function that returns a proxy
// (e.g. to cycle proxies).
exports.proxy = false;

// The maximum number of redirects to follow before calling fail()
exports.redirects = 3;

// This option is automatically filled with any extra arguments passed to the command line.
// exports.args;
