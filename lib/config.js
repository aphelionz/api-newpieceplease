var config = {};
	config.redis = {};

if(process.env.VCAP_SERVICES) {
	var services = JSON.parse(process.env.VCAP_SERVICES);
	config.redis.url = services['redis-2.2'][0].credentials.host;
	config.redis.port = services['redis-2.2'][0].credentials.port;
	config.redis.password = services['redis-2.2'][0].credentials.password;
	config.redis.name = services['redis-2.2'][0].credentials.name;
} else {	
	config.redis.url = 'localhost';
	config.redis.port = 10000;
	config.redis.password = 'dccf6f91-d5cf-4bb6-9c2a-f6ff87fd12e8';
	config.redis.name = '';
}

module.exports = config;
