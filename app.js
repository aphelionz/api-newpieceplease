var application_root = __dirname,
    express = require("express"),
    path = require("path"),
    fs = require("fs"),
    redis = require("redis"),
    config = require("./lib/config"),
    nodeio = require('node.io'),
    scraper_options = require('./jobs/options-global'),
    inflection = require('inflection'),
    http = require("http"),
    url = require("url"),
    npp = require("./lib/npp");

// Spin up express
var app = express();
app.configure(function () {
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(application_root, "public")));
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

// Connect to redis with config values
var client = redis.createClient(config.redis.port, config.redis.host);
    client.auth(config.redis.password);


/* TODO: Move to utils library */
function handleResponse(req, res, data) {
  // Hostname whitelist
  allowed_hostnames = ["localhost:4242", 'http://localhost', 'localhost'];
  if(allowed_hostnames.indexOf(req.headers.origin) >= 0) {
    res.setHeader('Access-Control-Allow-Origin', '*');
  }
  
  res.setHeader('Access-Control-Allow-Origin', '*');
  
  // application/json MIME type
  res.setHeader('Content-Type', 'application/json');
  res.setHeader('Cache-Control', 'no-cache');
  
  res.send(data);
  res.end();
}

/* TODO: Move to utils library */
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};


  /**********************/
 /* Main API Endpoints */
/**********************/
app.get('/pdfs/:instrument', function(req, res) {
  var instrument_name = req.params.instrument;
  var i,j,k
  var to_remove = [];
  var bad_words = [];

  client.keys('*' + instrument_name + '*', function(err, reply) {
    /*
     * Filtering out non-solo pieces with the following rules:
     *
     * 1) "orchestra" or "ensemble"
     * 2) more than one comma
     * 3) instrument name pluralized
     * 4) Ensemble size strings: duo, trio, quartet, quintet, sextet, septet, octet
     */

    bad_words.push("orchestra");
    bad_words.push("ensemble");
    bad_words.push("duo");
    bad_words.push("trio");
    bad_words.push("quartet");
    bad_words.push("quintet");
    bad_words.push("sextet");
    bad_words.push("septet");
    bad_words.push("octet");
    if(instrument_name !== "Tuba" && instrument_name !== "Drum") {
      bad_words.push(inflection.pluralize(instrument_name).toLowerCase());
    }

    for(i = 0; i < reply.length; i++) {
      for(j = 0; j < bad_words.length; j++) {
        if(reply[i].toLowerCase().indexOf(bad_words[j]) > 0) {
          to_remove.push(reply[i]);
          //console.log(reply[i] + " to be removed because it contains " + bad_words[j]);
        }
      }
      
      if(reply[i].split(',').length > 1) {
        to_remove.push(reply[i]);
        //console.log(reply[i] + " to be removed: too many commas!");
      }
    }

   for(k = 0; k < to_remove.length; k++) {
      if(reply.indexOf(to_remove[k]) >= 0) {
        reply.splice(reply.indexOf(to_remove[k]),1);
      }
      client.del(to_remove[k]); // used to clean house :D
    }

    client.sunion(reply, function(err, pdfs) {
      if(err) throw err;

      handleResponse(req, res, pdfs);
    });
  });
});


app.get('/checkpdf', function(req, res) {
  var pdf_url = req.query["url"];
  
  options = url.parse(pdf_url);
  options.method = "HEAD";
  
  http.get(options, function(http_res) {
	var pass = true;

	if(http_res.statusCode === "301" ) {
		pass = false;
	}
	
	if(http_res.headers['content-type'] === 'text/html') {
		pass = false;
	}
	
    handleResponse(req, res, { result: pass });
  }).on('error', function(e) {
    handleResponse(req, res, e);
  });
});

app.get('/instruments', function (req, res) {
  client.keys('*', function (err, reply) {
    json_response = {};
    json_response.size = reply.length;
    json_response.data = reply;
    
    handleResponse(req, res, json_response);
  });
});

/* TODO: Move to utils library */
app.get('/lastupdate', function (req, res) {
  fs.stat(__filename, function(err,data) {
    var time = {};
    time.mtime = Date.parse(data.mtime);
    
    handleResponse(req, res, time);
  });
});


/* TODO: Move to utils library */
app.get('/status', function(req, res) {
  client.set("epp_test_w00t_w00t", "Redis is working.");

  client.get("epp_test_w00t_w00t", function (err, redis_status) {
    if(err) {
      res.setHeader('Content-Type', 'application/json');
      res.send({ "error": err });
      res.end();
    }
    
    var status = {};
    status.redis = redis_status;
    status.client = req.headers;
    status.freescores_ticker = client.get("freescores:ticker");
    
    handleResponse(req, res, status);
  });
});


// Launch server
var server = app.listen(4242);

server.on('connection', function(socket) {
  socket.setTimeout(8 * 60 * 60 * 1000);
});
